const jwt = require("jsonwebtoken");
const boom = require('@hapi/boom');
const admin = require("firebase-admin");

admin.initializeApp({projectId: process.env.FIREBASE_PROJECT_ID});

const validateFirebaseToken = async (req, jwtToken) => {
  const firebaseToken = await admin.auth().verifyIdToken(jwtToken);
  req.tenantId = 'firebaseAuth';
  req.email = firebaseToken.email;
};

const validateOwnedJwtToken = (req, jwtToken, secret) => {
  const payload = jwt.verify(jwtToken, secret);
  req.tenantId = payload.tenantId;
  req.email = payload.userEmail;
  req.role = payload.role;
};

const formatToken = (authHeaders, next) => {
  const jwtToken = authHeaders;
  if (!jwtToken) {
    next(boom.badRequest('Auth_token_missing'));
    return;
  }
  const cleanToken = jwtToken.replace("Bearer ", "");
  return cleanToken;
};

const validateToken = async (req, res, next) => {
  const token = formatToken(req.headers.authorization, next);
  try {
    await validateFirebaseToken(req, token);
  } catch (error) {
    try {
      validateOwnedJwtToken(req, token, process.env.JWT_SECRET);
    } catch (error) {
      switch(error.name){
        case "JsonWebTokenError":
          next(boom.badRequest('Auth_token_invalid'));
        case "TokenExpiredError":
          next(boom.badRequest('Auth_token_expired'));
        default:
          next(boom.badRequest(error.name));
      }
    }
  }
  next();
}

module.exports = {
  validateToken,
};
