function endRequestMiddleware(data, req, res, next) {
  if (data instanceof Error) {
    res.status(400).send({errors: data.message});
    return;
  }
  res.status(200).send({data});
}

module.exports = {
  endRequestMiddleware
}
