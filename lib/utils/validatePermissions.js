const url = require('url');

const validatePermissions = (req, res, next) => {
    const { allowedModules } = req?.role ?? {};
    if (!allowedModules) {
        throw (new Error('Not_role_assigned'));
    }
    const parsedUrl = url.parse(req.originalUrl);
    const pathRequested = parsedUrl.pathname.split('/')[1];
    if (!allowedModules.includes(pathRequested)) {
        throw (new Error('Module_not_allowed'));
    }
    next();
};
module.exports = {
    validatePermissions
}