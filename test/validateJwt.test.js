/* eslint-disable no-undef */
const { assert } = require('chai');
process.env.JWT_SECRET = 'TESTINGKEY';
process.env.FIREBASE_PROJECT_ID = 'blind-spelling';
process.env.GOOGLE_APPLICATION_CREDENTIALS = './creds.json';
const { jwtUtils } = require('../index');

describe('JWT', () => {
  it('should fail if not auth headers over request', async () => {
    try {
      const validation = await jwtUtils.validateToken({headers: {}}, {}, (data) => data) ;
      assert.notExists(validation);
    } catch(err) {
      console.log(err)
      assert.equal(err.message, 'Auth_token_missing');
    }
  });

  it('should fail if signing key does not match', async () => {
    try {
      const req = {headers: {authorization: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2NjcwNzczNTV9.QBbnvUwHaS2oiiCUa9fALr1b9FVdPQo43vMYRM4d0ck'}};
      const validation = await jwtUtils.validateToken(req, {}, (data) => data);
      assert.notExists(validation);
    } catch(err) {
      assert.equal(err.message, 'Auth_token_invalid');
    }
  });

  it('should fail if token has expired', async () => {
    try {
      const req = {headers: {authorization: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2NjcwNzgxODMsImV4cCI6MTY2NzA3ODE5M30.sVjwq0YLGuJfGRxKv56p81iUygH95giq4hFFIGUUeTk'}};
      const validation = await jwtUtils.validateToken(req, {}, (data) => data);
      assert.notExists(validation);
    } catch(err) {
      assert.equal(err.message, 'Auth_token_expired');
    }
  });

  it('should be able to add tenantId payload over req', async () => {
    try {
      const req = {headers: {authorization: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyRW1haWwiOiJqb3JnZXF1aW50ZXJvODA2QGdtYWlsLmNvbSIsInJvbGUiOlsiYWdlbmRhIl0sInRlbmFudElkIjoidGVzdCIsImlhdCI6MTY5MTYyNDAwNn0.X03oN_FDZePUKHOcrnYYNFXBA3_6n0tY2scsD3avDvU'}};
      await jwtUtils.validateToken(req, {}, (data) => data);
      assert.deepEqual(req, {
        headers: {
          authorization: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyRW1haWwiOiJqb3JnZXF1aW50ZXJvODA2QGdtYWlsLmNvbSIsInJvbGUiOlsiYWdlbmRhIl0sInRlbmFudElkIjoidGVzdCIsImlhdCI6MTY5MTYyNDAwNn0.X03oN_FDZePUKHOcrnYYNFXBA3_6n0tY2scsD3avDvU'
        },
        tenantId: 'test',
        email: 'jorgequintero806@gmail.com',
        role: ["agenda"]
      });
    } catch(err) {
      assert.equal(err, undefined);
    }
  });

  
  // Para ejecutar este test, hay que quemarle un token generado desde el lado del cliente.
  // TODO - Validar si se puede generar el token dinamicamente desde aca.
  /*it('should be able to validate Firebase Token', async () => {
    try {
      const req = {headers: {authorization: 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjYyM2YzNmM4MTZlZTNkZWQ2YzU0NTkyZTM4ZGFlZjcyZjE1YTBmMTMiLCJ0eXAiOiJKV1QifQ.eyJuYW1lIjoiSk9SR0UgTFVJUyBRVUlOVEVSTyBNRURJTkEiLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tL2EvQUFjSFR0ZWlpQ3UxMG1HeWZfX2FiWXpLU3lKY1p2cXRnUjNqVzJaV0prSFU2b3FrTWtNPXM5Ni1jIiwiaXNzIjoiaHR0cHM6Ly9zZWN1cmV0b2tlbi5nb29nbGUuY29tL2JsaW5kLXNwZWxsaW5nIiwiYXVkIjoiYmxpbmQtc3BlbGxpbmciLCJhdXRoX3RpbWUiOjE2OTA5MzI1NTksInVzZXJfaWQiOiJQMXpOS29IM0N4T3JPOXRPekZRTmZTSW11dmwyIiwic3ViIjoiUDF6TktvSDNDeE9yTzl0T3pGUU5mU0ltdXZsMiIsImlhdCI6MTY5MDkzMjU1OSwiZXhwIjoxNjkwOTM2MTU5LCJlbWFpbCI6ImpvcmdlcXVpbnRlcm84MDZAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiYXBwbGUuY29tIjpbIjAwMTQyOC4yM2EyMzdmYTQzZGI0NjJlODAzMjZkMTM2MjMyY2IyMS4xNDAxIl0sImdvb2dsZS5jb20iOlsiMTEwNTAxNjY2NDQ1OTQzNzQyMjkwIl0sImVtYWlsIjpbImpvcmdlcXVpbnRlcm84MDZAZ21haWwuY29tIl19LCJzaWduX2luX3Byb3ZpZGVyIjoiZ29vZ2xlLmNvbSJ9fQ.P51-7MRp29AY-Y3WVAkrm8Phk6l0e3fQYid5mEJfhnIon0nf5JsMUGjQQ4EKIrSOOnJGY3TLj2RgUlrkf-x33r3TMScT5Q4ZIGMiRL-uhLNQHJgNSbC4LZfFkvMKbK8XsrwhCutxANHKhlsdy0Tk9Hmxlu18kDe2P58WAl7RExJXDi-8AnV7H8prxgRxec0wzuSQzIS7wjUoyl0vzyZVM-iBIaR_mzvWFY4ukVpD0iVOKK5gAKrHCahzOPuQqq9x7GBzMGvLWA_wL5hP-Xs_foD6yaBuhtzUQaMBojP9idiTl7XI0AWa2v2F233YKi8ljLf-Mny19fXT4JlpIYZy8A'}};
      await jwtUtils.validateToken(req, {}, (data) => data);
      assert.deepEqual(req, {
        headers: {
          authorization: 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjYyM2YzNmM4MTZlZTNkZWQ2YzU0NTkyZTM4ZGFlZjcyZjE1YTBmMTMiLCJ0eXAiOiJKV1QifQ.eyJuYW1lIjoiSk9SR0UgTFVJUyBRVUlOVEVSTyBNRURJTkEiLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tL2EvQUFjSFR0ZWlpQ3UxMG1HeWZfX2FiWXpLU3lKY1p2cXRnUjNqVzJaV0prSFU2b3FrTWtNPXM5Ni1jIiwiaXNzIjoiaHR0cHM6Ly9zZWN1cmV0b2tlbi5nb29nbGUuY29tL2JsaW5kLXNwZWxsaW5nIiwiYXVkIjoiYmxpbmQtc3BlbGxpbmciLCJhdXRoX3RpbWUiOjE2OTA5MzI1NTksInVzZXJfaWQiOiJQMXpOS29IM0N4T3JPOXRPekZRTmZTSW11dmwyIiwic3ViIjoiUDF6TktvSDNDeE9yTzl0T3pGUU5mU0ltdXZsMiIsImlhdCI6MTY5MDkzMjU1OSwiZXhwIjoxNjkwOTM2MTU5LCJlbWFpbCI6ImpvcmdlcXVpbnRlcm84MDZAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiYXBwbGUuY29tIjpbIjAwMTQyOC4yM2EyMzdmYTQzZGI0NjJlODAzMjZkMTM2MjMyY2IyMS4xNDAxIl0sImdvb2dsZS5jb20iOlsiMTEwNTAxNjY2NDQ1OTQzNzQyMjkwIl0sImVtYWlsIjpbImpvcmdlcXVpbnRlcm84MDZAZ21haWwuY29tIl19LCJzaWduX2luX3Byb3ZpZGVyIjoiZ29vZ2xlLmNvbSJ9fQ.P51-7MRp29AY-Y3WVAkrm8Phk6l0e3fQYid5mEJfhnIon0nf5JsMUGjQQ4EKIrSOOnJGY3TLj2RgUlrkf-x33r3TMScT5Q4ZIGMiRL-uhLNQHJgNSbC4LZfFkvMKbK8XsrwhCutxANHKhlsdy0Tk9Hmxlu18kDe2P58WAl7RExJXDi-8AnV7H8prxgRxec0wzuSQzIS7wjUoyl0vzyZVM-iBIaR_mzvWFY4ukVpD0iVOKK5gAKrHCahzOPuQqq9x7GBzMGvLWA_wL5hP-Xs_foD6yaBuhtzUQaMBojP9idiTl7XI0AWa2v2F233YKi8ljLf-Mny19fXT4JlpIYZy8A'
        },
        tenantId: 'firebaseAuth',
        email: 'jorgequintero806@gmail.com'
      });
    } catch(err) {
      assert.equal(err, undefined);
    }
  });*/

});
