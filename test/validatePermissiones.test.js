const { roleUtils } = require('../index'); 
const { assert } = require('chai');

describe('Roles', () => {

    it('should fail if not role property over request', async () => {
      try {
        const validation = roleUtils.validatePermissions({}, {}, (data) => data);
        assert.notExists(validation);
      } catch(err) {
        assert.equal(err.message, 'Not_role_assigned');
      }
    });

    it('should fail if role payload do not allow request module', async () => {
      try {
        const validation = roleUtils.validatePermissions({baseUrl: '/clients', role: {allowedModules: []} }, {}, (data) => data);
        assert.notExists(validation);
      } catch(err) {
        assert.equal(err.message, 'Module_not_allowed');
      }
    });

    it('should pass if role payload do allow request module', async () => {
      try {
        roleUtils.validatePermissions({baseUrl: '/clients', role: {allowedModules: ['clients']} }, {}, (data) => data);
      } catch(err) {
        assert.equal(err, undefined);
      }
    });
});